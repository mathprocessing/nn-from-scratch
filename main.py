import numpy as np

# -- Without loops --

inputs = [1, 2, 3, 2.5]
# we have 3 neurons
weights1 = [0.2, 0.8, -0.5, 1.0]
weights2 = [0.5, -0.91, 0.26, -0.5]
weights3 = [-0.26, -0.27, 0.17, 0.87]

bias1 = 2
bias2 = 3
bias3 = 0.5

weights = [weights1, weights2, weights3]
biases = [bias1, bias2, bias3]

outputs = [
    inputs[0] * weights1[0] + inputs[1] * weights1[1] + inputs[2] * weights1[2] + inputs[3] * weights1[3] + bias1
  , inputs[0] * weights2[0] + inputs[1] * weights2[1] + inputs[2] * weights2[2] + inputs[3] * weights2[3] + bias2
  , inputs[0] * weights3[0] + inputs[1] * weights3[1] + inputs[2] * weights3[2] + inputs[3] * weights3[3] + bias3
  ]

print(outputs) # [4.8, 1.21, 2.385]

# -- Without libraries (sentdex example) --

layer_outputs = [] # Output of current layer
for neuron_weights, neuron_bias in zip(weights, biases):
  neuron_output = 0 # Output of given neuron
  for n_input, weight in zip(inputs, neuron_weights):
    neuron_output += n_input * weight
  neuron_output += neuron_bias
  layer_outputs.append(neuron_output)

print(layer_outputs) # [4.8, 1.21, 2.385]

# -- With NUMPY --

def neuron_output(inputs, weights_and_bias):
  weights, bias = weights_and_bias
  return np.dot(inputs, weights) + bias

outputs_numpy = [neuron_output(inputs, pair) for pair in zip(weights, biases)]

print(outputs_numpy) # [4.8, 1.2099999999999997, 2.385]

# -- Using numpy features --

outputs_numpy_2 = np.dot(weights, inputs) + biases

print(outputs_numpy_2) # [4.8   1.21  2.385]