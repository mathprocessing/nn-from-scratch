import sys
import numpy as np
import matplotlib

print("Python:", sys.version)
print("Numpy:", np.__version__)
print("matplotlib: ", matplotlib.__version__)

# We need:
#
# Python: 3.7.7
# Numpy: 1.18.2
# matplotlib:  3.2.1